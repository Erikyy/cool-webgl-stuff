precision highp float;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform float time;
    
attribute vec3 position;
attribute vec2 uv;
attribute vec3 translate;
    
varying vec2 vUv;
varying float vScale;
varying float unit;
    
void main() {
    
    vec4 mvPosition = modelViewMatrix * vec4(translate, 1.0);
    unit = 1.0;
    vec3 vector = vec3(6.0 + unit, 6.0 + unit, 6.0 + unit);
    float scale = sin(vector.x * 6.0) + sin(vector.y * 5.0) + sin(vector.z * 4.0);
    vScale = scale;
    scale = scale * 0.3;
    mvPosition.xyz += position * scale;
    vUv = uv;
    gl_Position = projectionMatrix * mvPosition;
}