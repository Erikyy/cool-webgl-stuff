
import { 
    EffectComposer, 
    RenderPass,
    EffectPass, 
    DepthOfFieldEffect, 
    BlendFunction, 
    DepthEffect, 
    TextureEffect, 
    KernelSize, 
    BloomEffect, 
    OverrideMaterialManager, 
    SMAAEffect,
 } from 'postprocessing';

export default class ImageEffects {
    

    constructor () {
        
    }

    init(renderer, scene, camera) {
        this.scene = scene;
        this.camera = camera;
        this.composer = new EffectComposer(renderer);
        
        OverrideMaterialManager.workaroundenabled = true;
        let areaImg = new Image();
        let searchImg = new Image();
        areaImg.src = SMAAEffect.areaImageDataURL;
        searchImg.src = SMAAEffect.searchImageDataURL;
        const smaa = new SMAAEffect(searchImg, areaImg, 1);
        const dof = new DepthOfFieldEffect(camera, {
            focusDistance: 0.002,
            focalLength: 0.02,
            bokehScale: 3.0,
            height: 480,
        });
        //dof.circleOfConfusionMaterial.uniforms.focusDistance.value = 0.1;
        dof.renderToScreen = true;
        //dof.circleOfConfusionMaterial.uniforms.focalLenght.value = 0.038;
        dof.blurPass.kernelSize = KernelSize.MEDIUM;
        const depthEffect = new DepthEffect({
            blendFunction: BlendFunction.SKIP
        });

        const cocTexture = new TextureEffect({
            blendFunction: BlendFunction.SKIP,
            texture: dof.renderTargetCoC.texture
        });

        const blendMode = dof.blendMode;
        const cocMat = dof.circleOfConfusionMaterial;
        const effectPass = new EffectPass(camera, smaa, dof, cocTexture, depthEffect, new BloomEffect());

        this.composer.addPass(new RenderPass(this.scene, this.camera));
        this.composer.addPass(effectPass);
        blendMode.opacity.value = 1.0;
        blendMode.setBlendFunction(BlendFunction.NORMAL);

        cocMat.uniforms.focusDistance.value = 0.01;
        cocMat.uniforms.focalLength.value = 0.01;
    }

    render() {
        this.composer.render();
    }

    setSize(width, height) {
        this.composer.setSize(width, height);
    }

}