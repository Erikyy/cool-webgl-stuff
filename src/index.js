import Main from './js/main';
import './js/animate';
import animate from './js/animate';

const main = new Main();

main.init();
initialize();


function initialize() {
    animate((time) => {
        main.render(time);
    });
}
