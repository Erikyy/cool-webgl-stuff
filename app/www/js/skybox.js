import { Sky } from '../../node_modules/three/examples/jsm/objects/Sky.js';
import * as Three from '../../node_modules/three/src/Three.js';

export default class SkyBox {

    constructor (scene) {
        this.scene = scene;
    }

    init() {
        this.sky = new Sky();
        this.sky.scale.setScalar(4500);
        this.scene.add(this.sky);

        this.sun = new Three.Vector3();

        const params = {
            turbidity: 0, //haze
            rayleigh: 0.001, //rayleigh scattering 
            mieCoefficient: 0.005,
            mieDirectionalG: 0.07,
            inclination: 0.1,//sun angle
            azimuth: 0.25, //direction of a celestial object from the observer
            exposure: 0.001
        };

        const uniforms = this.sky.material.uniforms;
        uniforms[ "turbidity" ].value = params.turbidity;
        uniforms[ "rayleigh" ].value = params.rayleigh;
        uniforms[ "mieCoefficient" ].value = params.mieCoefficient;
        uniforms[ "mieDirectionalG" ].value = params.mieDirectionalG;

        const theta = Math.PI * (params.inclination - 0.5);
        const phi = 2 * Math.PI * (params.azimuth - 0.5);

        this.sun.x = Math.cos(phi);
        this.sun.y = Math.sin (phi) * Math.sin(theta);
        this.sun.z = Math.sin(phi) * Math.cos(theta);

        uniforms[ "sunPosition" ].value.copy(this.sun);
    }
}