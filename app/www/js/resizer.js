


export default class Resize { 

    constructor(camera, renderer, postprc, width, height, elements) {
        window.addEventListener("resize", () => {
            width = window.innerWidth;
            height = window.innerHeight;
            camera.aspect = width / height;
            camera.updateProjectionMatrix();
        
        
            renderer.setSize(width, height);
            postprc.setSize(width, height);
            elements.resize(width, height);
        
        });
    }
}