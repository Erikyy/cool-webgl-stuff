import * as Three from '../../node_modules/three/src/Three.js';
import SkyBox from './skybox.js';
import ImageEffects from './postprocess.js';
import LoadModels from './models.js';
import Particles from './particle.js';
import Resize from './resizer.js';
import Controller from './controls.js';
import Elements from './carousel.js';

//initializations
export default class Main {

    constructor () {


        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.renderer = new Three.WebGLRenderer({
            antialias: false,
            alpha: false,
            stencil: false,
            depth: false
        });

        this.scene = new Three.Scene();
        this.camera = new Three.PerspectiveCamera(55, this.width / this.height, 0.3, 20000);
        this.postprc = new ImageEffects();
        this.skybox = new SkyBox(this.scene);
        this.modelLoader = new LoadModels(this.scene);
        this.particles = new Particles();
        this.elements = new Elements(this.scene, this.camera);
        this.controller = new Controller(this.camera, this.elements);


    }

    init() {

        this.camera.position.set(-130, 0.10005, 312);
        this.rendererConf();
        var app = document.getElementById("app");
        app.appendChild(this.renderer.domElement)


        this.postprc.init(this.renderer, this.scene, this.camera);
        this.skybox.init()
        this.addDirectionalLight();
        this.modelLoader.initPlane();
        this.particles.dust(this.scene);

        new Resize(this.camera, this.renderer, this.postprc, this.width, this.height, this.elements);
        this.controller.initControls();
        //this.elements.init();
        this.elements.initElements();

    }

    addDirectionalLight() {
        const light = new Three.DirectionalLight(0xffffff, 0.75);
        light.position.set(0, 100, 50);
        light.castShadow = true;
        this.scene.add(light);
        const d = 1000;
        light.shadow.mapSize.width = 512;
        light.shadow.mapSize.height = 512;
        light.shadow.camera.near = 0.5;
        light.shadow.camera.far = 500;
        light.shadow.radius = 20;
        light.shadow.camera.left = -d;
        light.shadow.camera.right = d;
        light.shadow.camera.top = d;
        light.shadow.camera.bottom = -d;
    }

    render(time) {
        this.postprc.render();
        this.elements.render();
        this.modelLoader.animate();
        this.controller.update(time);

    }

    rendererConf() {
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);
        this.renderer.outputEncoding = Three.sRGBEncoding;
        this.renderer.toneMapping= Three.ACESFilmicToneMapping;
        this.renderer.toneMappingExposure = 0.5;
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = Three.PCFSoftShadowMap;
        this.renderer.shadowMap.autoUpdate = false;
        this.renderer.shadowMap.needsUpdate = true;
    }
}
