
import * as Three from '../../node_modules/three/src/Three.js';
import json from '../data.json';

export default class Elements {

    constructor(scene, camera) {
        this.scene = scene;
        this.camera = camera;
        this.offset = 0;
    }

    init() {
       
        var mat = new Three.MeshPhysicalMaterial({
            color: 0xc9c9c9,
            metalness: 0,
            roughness: 0,
            depthWrite: false,
            transparent: true,
            transmission: 0.3,

        });
        var startPos = -160;
        var geom = new Three.PlaneGeometry(10, 5);
        for (var i = 0; i < json.objects.length; i++) {
            var mesh = new Three.Mesh(geom, mat);
            mesh.position.set(startPos += 30, 0, 305);
            this.scene.add(mesh);
        }
        
    }

    render() {
        
    }

    resize(width, height) {
        
    }

    initElements() {
        
        var arr = document.createDocumentFragment();
        var menu = document.createDocumentFragment();
        
        for(var i = 0; i < json.objects.length; i++) {
            var content = json.objects[i];
            
            var elem = document.createElement('div');
            this.elementStyle(elem, content, i, this.offset);
            var menuitem = document.createElement('div');
            this.menuItemStyle(menuitem, i);
            
            this.offset += 100;
            arr.appendChild(elem);
            menu.appendChild(menuitem);
        }

        document.getElementById('nav-list').appendChild(arr);
        document.getElementById('slider-menu').appendChild(menu);
        
    }
    
    elementStyle(elem, content, i, offset) {
        elem.id = "slide";
        elem.className = "nav-item nav-item-"+i;
        elem.style.left = offset + '%';
        elem.style.display = 'inline-block'
        elem.style.transform = "translateX(" + offset +"%)";
        elem.innerHTML = `<div class="item-content">` + 
        '<h1 class="title"><a href="#">' + content.title +
        '</a></h1>' +

        `</div>`
    }

    menuItemStyle(menuitem, i) {
        menuitem.id = "menu-item-"+i;
        menuitem.className = "menu-"+i;
        menuitem.style.display = 'inline-block'
        menuitem.innerHTML = i;
    }

    clickPrev() {
        
        document.querySelectorAll('#slide').forEach(node => {
            console.log(node.style.transform);
            var transform = parseInt(node.style.transform.replace(/[^\d-.]/g, ''));
            var left = parseInt(node.style.left);
            console.log("before: "+transform);
            transform += 200;
            
            node.style.transform = "translateX(" + transform +"%)";
            console.log(transform);
        });
        
    }

    clickNext() {
       
        document.querySelectorAll('#slide').forEach(node => {
            console.log(node.style.transform);
            var transform = node.style.transform.replace(/[^\d-.]/g, '');
            var left = parseInt(node.style.left);
            console.log("before: "+transform);
            transform -= 200;
            var offset = transform - left;
            console.log(transform);
            node.style.transform = "translateX(" + transform +"%)";
            console.log("after: "+node.style.transform.replace(/[^\d-.]/g, ''));
        });
        
        
    }
}