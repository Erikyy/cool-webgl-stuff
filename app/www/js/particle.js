
import * as Three from '../../node_modules/three/src/Three.js';
import texture from 'url:../../www/assets/textures/circle.png'

export default class Particles {

    constructor() {
        this.container = new Three.Object3D();
    }

    dust(scene) {
        
        let particleCount = 10000;
        
        const circleGeom = new Three.CircleGeometry(1, 6);
    
        let geom = new Three.InstancedBufferGeometry();
        geom.index = circleGeom.index;
        geom.attributes = circleGeom.attributes;
            
        const translateArr = new Float32Array(particleCount * 3);
    
        for (let i = 0, i3 = 0, l = particleCount; i < l; i++, i3 += 3) {
            translateArr[i3 + 0] = Math.random() * 2 - 1;
            translateArr[i3 + 1] = Math.random() * 2 - 1;
            translateArr[i3 + 2] = Math.random() * 2 - 1;
        }
        
        geom.setAttribute('translate', new Three.InstancedBufferAttribute(translateArr, 3));
        
        const material = new Three.RawShaderMaterial({
            uniforms: {
                "map": {value: new Three.TextureLoader().load(texture)},
                "time": {value: 0.0}
            },
            vertexShader: require('../../assets/shaders/particle.vert'),
            fragmentShader: require('../../assets/shaders/particle.frag'),
            depthTest: true,
            depthWrite: true
        })
        
        let meshInstance = new Three.Mesh(geom, material);
        meshInstance.scale.set(500, 500, 500);
        meshInstance.position.set(0, 0, 200);
        scene.add(meshInstance)
    }
}