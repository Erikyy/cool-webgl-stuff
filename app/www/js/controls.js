import * as Tween from "@tweenjs/tween.js";

export default class Controller {

    constructor(camera, elements) {
        this.camera = camera;
        this.minThreshold = -140;
        this.maxThreshold = -30;
        this.moveDistance = 30;
        this.elements = elements;

    }

    initControls() {
        this.prev = document.getElementById("prev");
        this.next = document.getElementById("next");
        if (this.camera.position.x <= this.minThreshold + this.moveDistance) {
            this.prev.disabled = true;
        } else if (this.camera.position.x >= this.maxThreshold - this.moveDistance) {
            this.next.disabled = true;
        } else {
            this.prev.disabled = true;
            this.next.disabled = true;
        }
        this.prev.addEventListener("click", () => {
            if (this.camera.position.x > this.minThreshold) {
                this.elements.clickPrev();
                this.next.disabled = false;
                var coords = {
                    x: this.camera.position.x
                }
                new Tween.Tween(coords).to({ x: this.camera.position.x - this.moveDistance }).easing(Tween.Easing.Quadratic.Out).onStart(() => {
                    this.prev.disabled = true;
                    this.next.disabled = true;


                }).onUpdate(() => {
                    this.camera.position.set(coords.x, this.camera.position.y, this.camera.position.z);
                    this.camera.updateProjectionMatrix();

                }).start().onComplete(() => {
                    
                    this.next.disabled = false;
                    
                    if (this.camera.position.x <= this.minThreshold + this.moveDistance) {
                        this.prev.disabled = true;
                    } else {
                        this.prev.disabled = false;
                    }

                });

            } else {
                if (this.camera.position.x <= this.minThreshold) {
                    this.prev.disabled = true;
                }
            }
        });
        this.next.addEventListener("click", () => {
            if (this.camera.position.x < this.maxThreshold) {
                this.elements.clickNext();
                this.prev.disabled = false;

                var coords = {
                    x: this.camera.position.x
                }
                new Tween.Tween(coords).to({ x: this.camera.position.x + this.moveDistance }).easing(Tween.Easing.Quadratic.Out).onStart(() => {
                    this.prev.disabled = true;
                    this.next.disabled = true;


                }).onUpdate(() => {
                    this.camera.position.set(coords.x, this.camera.position.y, this.camera.position.z);
                    this.camera.updateProjectionMatrix();

                }).start().onComplete(() => {

                    console.log("complete");
                    this.prev.disabled = false;
                    if (this.camera.position.x >= this.maxThreshold - this.moveDistance) {
                        this.next.disabled = true;
                    } else {
                        this.next.disabled = false;
                    }
                });
            } else {
                if (this.camera.position.x >= this.maxThreshold) {
                    this.next.disabled = true;
                }

            }
        });
    }

    manual() {
        document.addEventListener('keydown', (e) => {
            e = e || window.event
            var keycode = e.key;
            if (keycode == 'ArrowUp') {
                this.camera.position.set(this.camera.position.x, this.camera.position.y, this.camera.position.z -= 1);
                this.camera.updateProjectionMatrix();
            } else if (keycode == 'ArrowDown') {
                this.camera.position.set(this.camera.position.x, this.camera.position.y, this.camera.position.z += 1);
                this.camera.updateProjectionMatrix();
            } else if (keycode == 'ArrowLeft') {
                this.camera.position.set(this.camera.position.x -= 1, this.camera.position.y, this.camera.position.z);
                this.camera.updateProjectionMatrix();
            } else if (keycode == 'ArrowRight') {
                this.camera.position.set(this.camera.position.x += 1, this.camera.position.y, this.camera.position.z);
                this.camera.updateProjectionMatrix();
            } else {

            }
        })
    }

    update(time) {
        if (this.camera.position.x > this.minThreshold + this.moveDistance || this.camera.position.x < this.maxThreshold - this.moveDistance)
            Tween.update(time);
    }

}