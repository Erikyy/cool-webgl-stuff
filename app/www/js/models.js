
import { GLTFLoader } from '../../node_modules/three/examples/jsm/loaders/GLTFLoader.js';
import * as Three from '../../node_modules/three/src/Three.js';
import plane from 'url:../../www/assets/models/Fractured.glb';

export default class LoadModels {
    constructor (scene) {
        this.loader = new GLTFLoader();
        this.scene = scene;
    }
    
    initPlane() {
        this.loader.load(plane, (gltf) => {
        
            var mat = new Three.MeshPhysicalMaterial({
                color: 0xc9c9c9,
                metalness: 0,
                roughness: 0,
                depthWrite: false,
                transparent: true,
                transmission: 0.7,
    
            });
            var mesh = gltf.scene.children[0];
            mesh.material = mat;
    
            mesh.position.set(0, -1, 300);
            mesh.receiveShadow = true;
            mesh.castShadow = true;
            mesh.scale.set(1, 1, 1);
            this.scene.add(mesh);
            
        }, function(xhr) {
            console.log("success");
        }, function (error) {
            console.log(error);
        });
    }

    animate() {

    }
}